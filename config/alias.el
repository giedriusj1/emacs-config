(defalias 'yes-or-no-p 'y-or-n-p)
(defalias 'describe-bindings 'helm-descbinds)

(defalias 'rel 'reload-emacs-config)
(defalias 'lp 'list-packages)
(defalias 'msf 'menu-set-font)
